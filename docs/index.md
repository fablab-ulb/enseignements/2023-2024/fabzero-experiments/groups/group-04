# Group and project presentation

## The ALED team

Our team is composed of :

- [Cams Lamon](https://camille-lamon-fablab-ulb-enseignements-2023-2024-7a46eb513030b8.gitlab.io/)
- [Julien Desuter](https://julien-desuter-fablab-ulb-enseignements-2023-202-535dc7e45c4a48.gitlab.io/)
- [Ali Bahja](https://ali-bahja-fablab-ulb-enseignements-2023-2024-fab-7873ad41e846a0.gitlab.io/)
- [Mamadou Diallo](https://diallo-mamadou-fablab-ulb-enseignements-2023-202-aedf16f9203541.gitlab.io/)

## Our Project abstract
As you may be aware, global access to healthcare is far from equitable.   
Our focus lies on the issue of Diabetes, with an emphasis on the elderly population.   
As technological advancements continue to grow exponentially, it's hard to keep up for older people who didn't grow up with access to all these electronics devices.      
Lots of them decide to rely on old glucometer as they are easier to use and understand.  
The problematic that we are working on is:     
    
_"How can we enhance the accessibility of diabetes management devices for older individuals, ensuring these technologies are user-friendly and seamlessly integrate into their daily routines to promote effective self-care?"_  
    
To address this problematic, we made some searches to understand the reluctance of older individuals to get the new/better medical devices?  
The major reasons we found are:  
- Limited financial resources    
- Health literacy  
- Limited availability of user-friendly medical devices   
- Dependence of medical devices on phones and computers    
- Lack of training on device usage   
  
When you add up everything, the main issue is that the devices often involves having phones and computers but also understanding how to use them.   
However, many older people might not be familiar with these technologies, making it tough for them to make the most of advanced medical devices.    
This lack of understanding not only makes it difficult for them to access optimal treatments but also makes health disparity between people even bigger. This emphasizes why it's so important to find ways to help older individuals use medical devices easily and take care of themselves better.   
Now, how can we help ?   

The solution we came up, was to make a machine that would continuously display information about the patient such as:     
- an arrow &#10138; that informs if the glucose is going up/down and how fast      
- the Blood glucose level  &#129656;   
- A suggested insuline dose to get (if needed)  &#128137;  
- A graph showing the last hours Blood glucose level &#128200;     
At the start, configuring the device is necessary. Yet, once it's properly set up, it operates independently, removing the need for any intervention from the patient.
   
We want it to be as easy as possible to assure that the patient understand what's going on by himself. 
It would also have to be the least expensive possible so that people who don't have easy access to primary healthcare could at least get one and understand how to treat themselves.   
This is our first sketch:  
![idea](images/idea.jpg)

## Final prototype 
![](images/prototype.jpg)
![](images/prototype.gif)


