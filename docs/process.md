# Processus
Hi! This page will be on the whole process of building our blood glucose display.    
If you're interested, you can go to our private pages (in index), and go look at how we made our group and chose our problematic :))  

## 1st Hackaton 

The week following the formation of our groups, we dedicated two days to a hackathon.   
You might be wondering what the hackathon was all about?   
Well, our task was to generate as many ideas as possible to address our identified issue (which, by the way, we excelled at).  
    
Initially we thought about destroying all the Mc-Donalds &#128163; but it felt a bit too...extreme?     
Next, we pondered making our own glucometer but this required chemical knowledge that we didn't have.  
Our third idea was to make a fake pancreas, but we didn't have the knowledge either ^^    
Our 4th concept was to make an insulin pump (but that's really dangerous)     
But that 4th concept led us to the idea of making a code that would tell you the dosage of insulin to give yourself without having to make the whole calculations etc...  
Then the FabLab managers came with great ideas too, and we got to our final idea which is to make a diabetic's glucometer data display :)) 

In the last half-hour on Tuesday, we presented our projects to the other groups to gather feedback.   
Our final idea, the Diabetic Data Display, received widespread approval and enthusiasm from everyone. &#129395;    
Interestingly, the McDonald's idea didn't quite resonate as much with the audience—it seemed our initial thoughts on that concept were indeed a bit unconventional! &#129325;  

Here were our first models to present to the other groups: 
![idées](images/idées%20.jpg)  
1) McDonald's idea  
2) Glucometer's idea    
3) Glucometer's idea 2.0    
4) Fake pancreas's idea    
5) Diabetic's glucometer data display     

## 1st Meeting 27/11  
We allocated ourselves a week to individually conduct research, aiming to develop a more comprehensive understanding of diabetes. This marked our initial meeting, and progress was swift! 😊  
Initially, we designated roles for the week, appointing a team leader and secretary for the current week and planning for the next ones.    
Week 1 (now): Secretary-Camille and Chief-Ali  
Week 2: Secretary-Mamadou and Chief-Julien   
Week 3: Secretary-Julien and Chief-Mamadou   
Week 4: Secretary-Ali and Chief-Camille   
The secretary's role is to make the documentation of the week (to make sure we don't fall behind)  
The Chief's role is to chose what we'll do for the next week and also make sure that everyone did what they had to!  
  
### How is our prototype going to work ? 
The data from a continuous Blood-glucose monitor (cgm) is systematically collected.   
This data is then transmitted through Bluetooth to a Raspberry Pi (Pic WH).   
The culmination of this setup results in the visualization of crucial information on an LED screen, embedded within a crafted structure produced by a 3D printer.   
This new system looks really promising for keeping an eye on important health info in real-time, and it doesn't limit itself to just one gadget.    
It's like a handy tool that helps you keep track of your health overall, giving you a full picture of what's going on.  
  
### To Do list 
<ul>
    <li><input type="checkbox" checked> Documented the process (Week 1)</li>
    <li><input type="checkbox" checked> Compiled a list of all necessary materials</li>
    <li><input type="checkbox" checked> Created a budget outline</li>
    <li><input type="checkbox" checked> Developed Arduino code (scheduled for a later week)</li>
    <li><input type="checkbox" checked> Established 3D printer settings and code</li>
    <li><input type="checkbox" checked> Calculated insulin dosage requirements</li>
    <li><input type="checkbox" checked> Established electrical connections between all components</li>
</ul>

### Problems and solutions of the week   
1) How are we going to recollect the CGM data ?      
&#10154; We decided to use bluetooth  
2) The raspberry pi pico doesn't have bluetooth, what are we going to work with then ?    
&#10154; We will work with the Raspberry pi pico WH (cheapest option)  
3) What do we want to display ?     
&#10154; Blood glucose level + arrow that shows if it's going up/down + recommended insulin dosage    
&#10154; Two screen alternating, one with a graph of the last hours of blood-glucose level and another one with the blood glucose level + insulin dosage recommended    
4) What should the size of the screen be ?     
&#10154; We chose a 2.4 inches touch-screen   
5) Prices     
&#10154; The Raspberry cost 9 euros and the screen cost 13,10 euros, so we're at a 22,10 euros budget    

### Next week's objective   
- Find the material + buy it   
- Find the calculations for insulin dosage   
- Start the OPENSCAD code   
- Find a Dexcom   

## 2nd meeting 6/12
The first step was to make a checklist of what we wanted to do during the reunion and also ensure that everyone did their last week's objective!
And we did! so that's great :))  
### Checklist of the meeting 
<ul>
    <li><input type="checkbox" checked> Look at our progress</li>
    <li><input type="checkbox" checked> Talk about what's next?</li>
    <li><input type="checkbox" checked> Choose the parameters for the box</li>
    <li><input type="checkbox" checked> Make the electrical connection</li>
    <li><input type="checkbox" checked> Make reservations for the printer</li>
    <li><input type="checkbox" checked> Verify that the screen is working</li>
</ul>

### Session walkthrough
We got our [screen](https://l.messenger.com/l.php?u=https%3A%2F%2Fwww.otronic.nl%2Fnl%2F28inch-tft-touch-screen-320x240-ili9341-spi.html&h=AT2T_EO-YwWHVnW60UcTNtTqIWFipagZ7btqmIJzCd12Qx7CKtGqyaXsjOLnsplGM_EtsUHNd7hJ-gIzUEZk1uxLMULHiOFdh0C_2gH15CDDpLOd-qZNFFfBgHQNt7K3HIKLOg) and [Raspberry-Pi-Pico-WH](https://l.messenger.com/l.php?u=https%3A%2F%2Fshop.mchobby.be%2Ffr%2Fpico-rp2040%2F2435-pico-wh-wireless-rp2040-2-coeurs-wifi-bluetooth-avec-connecteurs-3232100024359.html&h=AT2T_EO-YwWHVnW60UcTNtTqIWFipagZ7btqmIJzCd12Qx7CKtGqyaXsjOLnsplGM_EtsUHNd7hJ-gIzUEZk1uxLMULHiOFdh0C_2gH15CDDpLOd-qZNFFfBgHQNt7K3HIKLOg) delivered     
![commande](images/pièces.jpg)  
The first step during the meeting was to go around everyone's work and establish our objectives for next week!  
Julien did the _OPENSCAD_ code for the box, and we only had to choose the final parameters before printing it. 
He and Camille did that during the reunion and this is what they came up with:  
![dimensions](images/Dimensions.jpg)    
Epsilon &#949;: measure to add so that the pieces interlock perfectly (parametrization) = 1mm  
To choose Epsilon, they considered the size of the screen and the ON/OFF button but also the fact that if the screen space of the box is exactly the same as the one of the screen, they won't fit!   
The first prototype will be printed friday by Camille and these are the codes:   
- Front of the box   
```openscad
// Parameters for the open box
box_length = 92;
box_width = 65;
box_height = 55;

// Parameters for the inner box
inner_length = 82;
inner_width = 55;
inner_height = 50;

// Parameters for the screen
screen_length = 61;
screen_width = 44;

// Parameters for the additional small hole
small_hole_width = 10;
small_hole_height = 10;

// Create the open box with a rectangular hole in the front panel
difference() {
    // Outer box
    cube([box_length, box_width, box_height]);

    // Inner box (positioned to leave the back open)
    translate([(box_length - inner_length) / 2, (box_width - inner_width) / 2, 0])
        cube([inner_length, inner_width, inner_height]);

    // Front panel with rectangular hole for the screen
    translate([(box_length - screen_length) / 2, (box_width - screen_width) / 2, 0])
        cube([screen_length, screen_width, box_height]);

    // Small hole on the left side of the screen hole
    translate([(box_length - screen_length) / 2 - small_hole_width, (box_width - small_hole_height) / 2, 0])
        cube([small_hole_width, small_hole_height, box_height]);
}
```  
- Back of the box  
```openscad 
// Parameters for the back panel
back_panel_thickness = 10;
box_length = 92;
box_width = 65;

// Back panel
cube([box_length, box_width, back_panel_thickness]);
``` 
When interlinked, it should look like this: 
<div class="sketchfab-embed-wrapper">
  <iframe
    title="Home display"
    frameborder="0"
    allowfullscreen
    mozallowfullscreen="true"
    webkitallowfullscreen="true"
    allow="autoplay; fullscreen; xr-spatial-tracking"
    xr-spatial-tracking
    execution-while-out-of-viewport
    execution-while-not-rendered
    web-share
    src="https://sketchfab.com/models/ee258c02f51641babe7c04768cdffb08/embed"
  ></iframe>
  <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a
      href="https://sketchfab.com/3d-models/home-displayer-ee258c02f51641babe7c04768cdffb08?utm_medium=embed&utm_campaign=share-popup&utm_content=ee258c02f51641babe7c04768cdffb08"
      target="_blank"
      rel="nofollow"
      style="font-weight: bold; color: #1CAAD9;"
    >
      Home display
    </a>
    by
    <a
      href="https://sketchfab.com/Camsouille?utm_medium=embed&utm_campaign=share-popup&utm_content=ee258c02f51641babe7c04768cdffb08"
      target="_blank"
      rel="nofollow"
      style="font-weight: bold; color: #1CAAD9;"
    >
      Cams
    </a>
    on
    <a
      href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=ee258c02f51641babe7c04768cdffb08"
      target="_blank"
      rel="nofollow"
      style="font-weight: bold; color: #1CAAD9;"
    >
      Sketchfab
    </a>
  </p>
</div>

Ali ordered the screen + Raspberry, and also made his research on how the cgm works.      
During the meeting, him and Mamadou made the electrical connections between the screen and the Raspberry which was a hard and long task! Thankfully they were able to make it work &#129299;   
![connexion](images/connexion.jpg)

### Diabetic's information 
We made a little questionnaire to get as much information as possible from diabetics to be sure that we were considering the good factors! 
These were the most important information that we've gathered:       
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Most used Cgm</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>

<h2>Most used Cgm</h2>

<canvas id="resizedPieChart" width="180" height="180"></canvas>

<script>
    // Sample data
    const data = [66.6, 33.3];
    const labels = ['Freestyle', 'Guardian'];

    // Create a resized pie chart using Chart.js
    const ctxResized = document.getElementById('resizedPieChart').getContext('2d');
    const resizedPieChart = new Chart(ctxResized, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [{
                data: data,
                backgroundColor: ['rgba(255, 99, 132, 0.7)', 'rgba(54, 162, 235, 0.7)'],
            }]
        },
        options: {
            responsive: false, // Disable responsiveness
            maintainAspectRatio: false, // Disable aspect ratio
        }
    });
</script>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Most used glucometer</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
</body>
<body>

<h2>Most used glucometer</h2>

<canvas id="dynamicPieChart" width="180" height="180"></canvas>

<script>
    // Sample data
    const totalAnswers = 8;
    const freestyleCount = 2;
    const accuCheckCount = 4;
    const medtronicCount = 2;

    // Calculate percentages
    const freestylePercentage = (freestyleCount / totalAnswers) * 100;
    const accuCheckPercentage = (accuCheckCount / totalAnswers) * 100;
    const medtronicPercentage = (medtronicCount / totalAnswers) * 100;

    // Create a dynamic pie chart using Chart.js
    const ctxDynamic = document.getElementById('dynamicPieChart').getContext('2d');
    const dynamicPieChart = new Chart(ctxDynamic, {
        type: 'pie',
        data: {
            labels: ['Freestyle', 'Accu-check', 'Medtronic'],
            datasets: [{
                data: [freestylePercentage, accuCheckPercentage, medtronicPercentage],
                backgroundColor: ['rgba(255, 99, 132, 0.7)', 'rgba(54, 162, 235, 0.7)', 'rgba(255, 206, 86, 0.7)'],
            }]
        },
        options: {
            responsive: false, // Disable responsiveness
            maintainAspectRatio: false, // Disable aspect ratio
        }
    });
</script>   
</body>
</html>

It seems like in Europe, people use mostly Freestyle's glucometer and not Dexcom, so we're going center our project on making the code for a freestyle device and not a Dexcom.   
### Next week's objectives 
     
<ul>
    <li><input type="checkbox" checked> Print the prototype</li>
    <li><input type="checkbox" checked> Make the circuit </li>
    <li><input type="checkbox" checked> Make the documentation</li>
    <li><input type="checkbox" checked> Start the Arduino code</li>
    <li><input type="checkbox" checked> Find Blood-glucose data</li>
    <li><input type="checkbox" checked> Finish OPENSCAD's code</li>
</ul>  

  



         
## Big turn of events  
    
We initially believed that the screen turning on meant progress and that the circuit was close to completion.  
However, this was not the case...  
The screen remained blank, indicating a circuit malfunction.  
Despite Ali's determined efforts to troubleshoot, none of the attempts were successful.  
It became clear at that moment that we would not be able to complete the project within the allocated time! 
With only two weeks remaining, we were confronted with the task of completing the API, writing the Arduino code, and finalizing the circuit.   
All of this needed to be achieved while simultaneously managing our responsibilities in other classes. This added a lot of pressure to an already demanding situation and I (Cams) CANT WORK UNDER PRESSURE AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH &#128561; &#128561; &#128561;   
     
## Meeting Wednesday 13th of December     
   
Two days ago, we had a meeting with Denis, and he helped us find a way to not lose too much time while trying to debug the circuit.
We're having problems linking the Raspberry to the screen but when we tried with an Arduino Uno it became a lot easier!
So we made two pairs, one made of Julien and Cams and the other made of Mamadou and Ali.
Julien and Cams will find raw data from a freestyle cgm and make a MicroPython code for data analysis while Mamadou and Ali will work on the circuit + work on the display of the screen with the Arduino Uno.
When we were all ok with what we had to do, we started working, and we stayed at the fablab for 4 hours!! It was really tiring, but we made huge steps :))</p>

<ul>
  <li><input type="checkbox" checked> Micropython code</li>
  <li><input type="checkbox" checked> Finish the first 3D prototype</li>
  <li><input type="checkbox" checked> Make a better prototype on OpenScad</li>
  <li><input type="checkbox" checked> Print the better prototype (Friday)</li>
  <li><input type="checkbox" checked> Find the library for the Arduino Uno</li>
  <li><input type="checkbox" checked> Understand how to display on the screen</li>
</ul>

This is what the micropython code looks like:  
```python
import pandas as pd
import matplotlib.pyplot as plt 

# Replace 'C:/Users/lamon/polar/your_file.csv' with the actual path to your CSV file
file_path = 'C:/Users/lamon/polar/glucose_20-10-2023.csv'

# Read the CSV file into a pandas DataFrame
data = pd.read_csv(file_path, header=1)

# Convert the 'Device Timestamp' column to datetime format
data['Device Timestamp'] = pd.to_datetime(data['Device Timestamp'], format='%d/%m/%Y %H:%M')

# Print the minimum and maximum timestamps in the data
print("Min Device Timestamp:", data['Device Timestamp'].min())
print("Max Device Timestamp:", data['Device Timestamp'].max())

# Calculate the timestamp 24 hours ago from the maximum timestamp in the data
twenty_four_hours_ago = data['Device Timestamp'].max() - pd.DateOffset(hours=24)

# Print the calculated timestamp 8 hours ago
print("Device Timestamp 24 Hours Ago:", twenty_four_hours_ago)

# Select rows where the 'Device Timestamp' is within the last 24 hours
selected_data = data[data['Device Timestamp'] >= twenty_four_hours_ago]

sel_gluc_mg = selected_data['Historic Glucose mmol/L']*18

# Display the selected data or perform further analysis
print(selected_data)

# Find the row with the maximum timestamp (latest entry)
latest_entry = data.loc[data['Device Timestamp'].idxmax()]

# Extract relevant information (replace with actual column names)
historic_glucose_value = latest_entry['Historic Glucose mmol/L']
scan_glucose_value = latest_entry['Scan Glucose mmol/L']
timestamp = latest_entry['Device Timestamp']

# Check if historic glucose data is available and not NaN, otherwise use scan glucose data
if pd.notna(historic_glucose_value) and not pd.isna(historic_glucose_value):
    glucose_value = historic_glucose_value
else:
    glucose_value = scan_glucose_value if pd.notna(scan_glucose_value) else "Data Not Available"

# Print the latest glucose data
print("Latest Glucose Data:")
print(f"Glucose: {glucose_value} mmol/L")
print(f"Timestamp: {timestamp}")

# Save the latest glucose data to a text file
latest_glucose_data = f"Latest Glucose Data:\nGlucose: {glucose_value} mmol/L\nTimestamp: {timestamp}"
with open('latest_glucose_data.txt', 'w') as file:
    file.write(latest_glucose_data)

fig = plt.figure()
ax = fig.add_subplot(111)
green = sel_gluc_mg.mask(data["Historic Glucose mmol/L"]*18 > 120)
red = sel_gluc_mg.mask(data["Historic Glucose mmol/L"]*18 <= 120)
ax.plot(selected_data['Device Timestamp'], sel_gluc_mg, color='y')
ax.plot(selected_data['Device Timestamp'], red, color='r')
ax.plot(selected_data['Device Timestamp'], green, color='g')

plt.ylabel('Glucose (mg/dl)')
plt.xlabel('Time (heures)')
ax.xaxis.set_ticklabels(['-24', '-21', '-18', '-15', '-12', '-9', '-6', '-3', 'NOW'])
plt.savefig('graph.png')
plt.show()
print(data.columns)  
```
It displays a graph of the last 24 hours blood glucose data (in a PNG.file) and also gives the last blood glucose data taken (in a txt. File).  
What we are going to try to do for next week is to transfer the output of the micropython code to the C code that talks with the Arduino Uno.  
If we take the arduino Uno, we also need to change the 3D box because there's going to be cables.     
The box looks like that right now:  
![firstbox](images/firstboxx.jpg)  
We joined the two parts of the box by drilling holes with a bit size 3, and then we screwed it :))  
This is the new OPENSCAD box that will be printed friday!!! 
```openscad
// Parameters for the open box
box_length = 92;
box_width = 65;
box_height = 26;

// Parameters for the inner box
inner_length = 82;
inner_width = 55;
inner_height = 21;

// Parameters for the screen
screen_length = 64;
screen_width = 44;

// Parameters for the additional small hole
small_hole_width = 6;
small_hole_height = 6; 

//Parameters for the cables hole
cable_hole_width = 41; 
cable_hole_height = 11; 
cable_hole_distance = 9.75;  // Distance from the upper side
cable_hole_distance_left = 4.9; // Distance from the left side 

// Create the open box with a rectangular hole in the front panel
difference() {
    // Outer box
    cube([box_length, box_width, box_height]);

    // Inner box (positioned to leave the back open)
    translate([(box_length - inner_length) / 2, (box_width - inner_width) / 2, 0])
        cube([inner_length, inner_width, inner_height]);

    // Front panel with rectangular hole for the screen
    translate([(box_length - screen_length) / 2, (box_width - screen_width) / 2, 0])
        cube([screen_length, screen_width, box_height]);

    // Small hole on the left side of the screen hole
    translate([(box_length - screen_length) / 2 - small_hole_width, (box_width - small_hole_height) / 2, 0])
        cube([small_hole_width, small_hole_height, box_height]);
   // Big rectangle on the front side 
    trans = screen_width - cable_hole_distance_left; 
    translate([0,trans, box_height-cable_hole_distance])
        cube([box_height,cable_hole_width,cable_hole_height], center=true); 
}
```
The box should look like this:  
<div class="sketchfab-embed-wrapper"> <iframe title="Avant Boite2.0" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share src="https://sketchfab.com/models/95f45f53e86246e29ab1a435a409da39/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/avant-boite20-95f45f53e86246e29ab1a435a409da39?utm_medium=embed&utm_campaign=share-popup&utm_content=95f45f53e86246e29ab1a435a409da39" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> Avant Boite2.0 </a> by <a href="https://sketchfab.com/Camsouille?utm_medium=embed&utm_campaign=share-popup&utm_content=95f45f53e86246e29ab1a435a409da39" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> Cams </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=95f45f53e86246e29ab1a435a409da39" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>  
   
We took the measure by making a cable hole with a Drill  
![holes](images/hole.jpg)   
    
### Next Week's objectives </p>  
  
<ul>
  <li><input type="checkbox" checked> Make the documentation</li>
  <li><input type="checkbox" checked> Join the micropython code to the C code</li>
  <li><input type="checkbox" checked> Continue trying to join the raspberry WH to the screen</li>
  <li><input type="checkbox" checked> Make the PowerPoint</li>
</ul>  
   
## Hackaton 21/12/2023
On the 21st of december, we all met up for the whole day to make a last fast hackaton.  
The first thing we did was to try our last box prototype with the screen+Arduino Uno.   
It seems like everything is okay except the little hole on the upper side who needs +1mm  
We sanded down the hole, and we were able to make it fit.  
We made the two usual groups: Julien+Camille and Ali+Mamadou    
Ali and mamadou worked on the C code while Julien and Camille worked on the presentation + documentation and the box.  
New prototype: 
```openscad
// Parameters for the open box
box_length = 92;
box_width = 65;
box_height = 26;

// Parameters for the inner box
inner_length = 82;
inner_width = 55;
inner_height = 21;

// Parameters for the screen
screen_length = 64;
screen_width = 44;

// Parameters for the additional small hole
small_hole_width = 8;
small_hole_height = 8; 

//Parameters for the cables hole
cable_hole_width = 41; 
cable_hole_height = 15; 
cable_hole_distance = 13;  // Distance from the upper side
cable_hole_distance_left = 4.9; // Distance from the left side 

// Create the open box with a rectangular hole in the front panel
difference() {
    // Outer box
    cube([box_length, box_width, box_height]);

    // Inner box (positioned to leave the back open)
    translate([(box_length - inner_length) / 2, (box_width - inner_width) / 2, 0])
        cube([inner_length, inner_width, inner_height]);

    // Front panel with rectangular hole for the screen
    translate([(box_length - screen_length) / 2, (box_width - screen_width) / 2, 0])
        cube([screen_length, screen_width, box_height]);

    // Small hole on the left side of the screen hole
    translate([(box_length - screen_length) / 2 - small_hole_width, (box_width - small_hole_height) / 2, 0])
        cube([small_hole_width, small_hole_height, box_height]);
   // Big rectangle on the front side 
    //translate([0, (box_width - cable_hole_width) / 2, cable_hole_distance])
    //    cube([box_height, cable_hole_width, cable_hole_height]);
    trans = screen_width - cable_hole_distance_left;
    translate([0,trans,box_height -cable_hole_distance])
    cube([box_height, cable_hole_width, cable_hole_height],center = true);
   
}
```
### Blocus 
After the presentation on the 22nd of december, we were given some advice on what to add/change and how to navigate the last 3 weeks leading to the final presentation on the 16th of January.  
Our remaining tasks included: finish the documentation, refining the presentation, find a way to display the last glucose data and also talk about what we could have done if we had more time.
We've all worked on every task, and we were able to finish on time!    
This was our final C++ code for the Arduino:  
```C
#include <UTFTGLUE.h>

UTFTGLUE myGLCD(0, A2, A1, A3, A4, A0); // all dummy args

void setup()
{
  randomSeed(analogRead(0));
  myGLCD.InitLCD();
  Serial.begin(9600);
}
void drawScale()
{
  myGLCD.setColor(255, 255, 255);
  myGLCD.drawLine(27, 220, 27, 20); // Vertical line

  // Draw scale lines and labels
  for (int i = 0; i <= 20; i++)
  {
    int y = 220 - i * 10;
    myGLCD.drawLine(20, y, 25, y);  
    myGLCD.printNumI(i * 10, 5, y - 4); // Label
  }
}
void drawBox(int x, int y, int width, int height, int value, const char *qualifier) {
  // Draw the box
  if (strcmp(qualifier,"High")){
      myGLCD.setColor(0,255, 0);
  }
  else{
    myGLCD.setColor(255, 0, 0);
  }
  //myGLCD.fillRect(x, y, x + width, y + height);

  // Draw the value
  
  
  myGLCD.setFont(BigFont);
  myGLCD.printNumI(value, x + 60, y + 40);
  myGLCD.print("mg/dl", x + 110, y + 40);

  // Draw the qualifier
  myGLCD.setFont(SmallFont);
  myGLCD.print(qualifier, x + 100, y + 80);
  myGLCD.setFont(SmallFont);
}

void loop()
{
  myGLCD.setFont(SmallFont);
  int numPoints = 10;
  int xScale = 35;
  int yScale = 1;
  int threshold = 120;

  int data[numPoints];

  // Generate random glucose level data (replace this with your actual data source)
  for (int i = 0; i < numPoints; i++)
  {
    data[i] = random(80, 150); // Random values between 80 and 120 (adjust as needed)
  }

  myGLCD.clrScr();
  myGLCD.setColor(255, 255, 255);
  drawScale();
  //myGLCD.drawLine(10, 120, 310, 120);
  //myGLCD.drawLine(10, 10, 10, 120);
  

  for (int i = 0; i < numPoints - 1; i++)
  {
    int x1 = 26 + i * xScale;
    int y1 = 120 - (data[i]-100) * yScale;
    int x2 = 26 + (i + 1) * xScale;
    int y2 = 120 - (data[i + 1]-100) * yScale;
      if ((data[i] + data[i + 1]) / 2 < threshold -5)
    {
      myGLCD.setColor(0, 255, 0); // Green if under the threshold
    }
    else if ((data[i] + data[i + 1]) / 2 <= threshold+5 &&   threshold -5 <=(data[i] + data[i + 1]) / 2 )
    {
      myGLCD.setColor(255, 255, 0); // Yellow if within the threshold
    }
    else // if ((data[i] + data[i + 1]) / 2 > threshold)
    {
      myGLCD.setColor(255, 0, 0); // Red if over the threshold
    }
    

    myGLCD.drawLine(x1, y1, x2, y2);

    Serial.print("data ");
    Serial.println(data[i]);
    
  }

  delay(5000);
  myGLCD.clrScr();
  myGLCD.setColor(255, 255, 255);
  int currentValue = data[numPoints - 1]; // Get the last value for demonstration
  const char *qualifier = (currentValue < threshold+5) ? "Normal" : "High"; // Adjust as needed

  // Draw the box with the value and qualifier
  drawBox(40, 40, 240, 160, currentValue, qualifier);
  delay(5000);
  myGLCD.clrScr();

}
   
``` 
## Final Prototype
### Box
![](images/prototype.jpg)

### Graph
![](images/graphPrototype.jpg)

### Normal Glycaemia value
![](images/goodValue.jpg)

### High Glycaemia value
![](images/badvalue.jpg)


## What could we have done better ?   
First and foremost, we really should have more thought about our prototype before printing them...     
We've printed a lot of prototypes and that was a waste of material!  
These are all the prototypes we've printed:  
![prototypes](images/prototypes.jpg)    
Another thing we could have done better would have been to directly go with an Arduino Uno so that we wouldn't have lost that much time on trying to make the electrical connections. This spare time could have been used on making an API for the freestyle's glucometer.    

## How can this final prototype be improved ? 
In the end, we were not able to make an API as we didn't have enough time.  
It wouldn't have been that hard to make as there already are some online!   
Without an API, the prototype has to stay connected to a computer... which is what we were working on getting rid of :( but we think that our prototype can still find hiw way in the world by just working on it a little bit more!!  

## Final words
Even if we're a bit sad that we were not able to get through with our initial idea, we can still be proud of what we've done as we've all expanded our expertise on this disease.   
We all learned something during this project, from basic coding concepts to insights into medical devices, and even how to comprehend a data sheet.     
We were a quite diverse area of study group, and we made the best of it :)    
Thank you everyone!!!!   
Ali, Camille, Julien and Mamadou 


