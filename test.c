include <stdio.h>
include <stdlib.h>
include <csv.h>
include <time.h>

int main() {
    // CSV parsing example using libcsv
    csv_parser csv;
    csv_init(&csv, 0);


FILE *csvFile = fopen("glucose_20-10-2023.csv", "r"); and use csv_parse_file

    // Date-Time operations example using strptime
    const char *timestamp_str = "2023-10-19 15:57:00";
    struct tm timestamp;
    strptime(timestamp_str, "%Y-%m-%d %H:%M:%S", &timestamp);

    // Plotting example using gnuplot
    FILE *gnuplotPipe = popen("gnuplot -persistent", "w");
    fprintf(gnuplotPipe, "plot 'your_data_file.csv' using 1:2 with lines\n");
    fclose(gnuplotPipe);

    return 0;
}
